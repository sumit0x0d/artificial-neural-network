#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "artificial-neural-network.h"
#include "matrix/matrix.h"

ArtificialNeuralNetwork *ArtificialNeuralNetwork_Create(char *datasetName, size_t inputNeuronCount,
	size_t outputNeuronCount, size_t hiddenLayerCount, size_t *hiddenNeuronCount, double learningRate,
	double (*initilizeWeight)(double fanIn, double fanOut))
{
	ArtificialNeuralNetwork *neuralNetwork = NULL;
	Matrix **hiddenLayer= NULL;

	neuralNetwork = (ArtificialNeuralNetwork *)malloc(sizeof (ArtificialNeuralNetwork));
	assert(neuralNetwork);
	hiddenLayer = (Matrix **)malloc((hiddenLayerCount + 1) * sizeof (Matrix *));
	assert(hiddenLayer);
	hiddenLayer[0] = Matrix_Create(inputNeuronCount, hiddenNeuronCount[0]);
	assert(hiddenLayer[0]);
	for (size_t i = 1; i < hiddenLayerCount - 1; i++) {
		hiddenLayer[i] = Matrix_Create(hiddenNeuronCount[i - 1], hiddenNeuronCount[i]);
		assert(hiddenLayer[i]);
	}
	hiddenLayer[hiddenLayerCount - 1] = Matrix_Create(hiddenNeuronCount[hiddenLayerCount - 1], outputNeuronCount);
	assert(hiddenLayer[hiddenLayerCount - 1]);
	Matrix_WeightInitilization(hiddenLayer[0], inputNeuronCount, hiddenNeuronCount[0], initilizeWeight);
	for (size_t i = 1; i < hiddenLayerCount - 1; i++) {
		Matrix_WeightInitilization(hiddenLayer[i], hiddenNeuronCount[i - 1], hiddenNeuronCount[i],
			initilizeWeight);
	}
	Matrix_WeightInitilization(hiddenLayer[hiddenLayerCount - 1], hiddenNeuronCount[hiddenLayerCount - 1],
		outputNeuronCount, initilizeWeight);
	neuralNetwork->datasetName = datasetName;
	neuralNetwork->inputNeuronCount = inputNeuronCount;
	neuralNetwork->outputNeuronCount = outputNeuronCount;
	neuralNetwork->hiddenLayerCount = hiddenLayerCount;
	neuralNetwork->hiddenNeuronCount = hiddenNeuronCount;
	neuralNetwork->hiddenLayer = hiddenLayer;
	neuralNetwork->learningRate = learningRate;

	return neuralNetwork;
}

void ArtificialNeuralNetwork_Destroy(ArtificialNeuralNetwork *neuralNetwork)
{
	for (size_t i = 0; i < neuralNetwork->hiddenLayerCount; i++) {
		Matrix_Destroy(neuralNetwork->hiddenLayer[i]);
	}
	free(neuralNetwork->hiddenLayer);
	free(neuralNetwork);
}

void ArtificialNeuralNetwork_Train(ArtificialNeuralNetwork *neuralNetwork, Matrix *inputLayer, Matrix *outputLayer,
	double (*activate)(double weight))
{
	Matrix **matrix = NULL;

	matrix = (Matrix **)malloc((neuralNetwork->hiddenLayerCount + 1) * sizeof (Matrix *));
	assert(matrix);
	matrix[0] = Matrix_Multiplication(inputLayer, neuralNetwork->hiddenLayer[0]);
	assert(matrix[0]);
	Matrix_Activation(matrix[0], activate);
	for (size_t i = 1; i < neuralNetwork->hiddenLayerCount - 1; i++) {
		matrix[i] = Matrix_Multiplication(matrix[i - 1], neuralNetwork->hiddenLayer[i]);
		assert(matrix[i]);
		Matrix_Activation(matrix[i], activate);
	}
	matrix[neuralNetwork->hiddenLayerCount - 1] =
		Matrix_Multiplication(matrix[neuralNetwork->hiddenLayerCount - 1], outputLayer);
	assert(matrix[neuralNetwork->hiddenLayerCount - 1]);
	Matrix_Activation(matrix[neuralNetwork->hiddenLayerCount - 1], activate);
}

void ArtificialNeuralNetwork_Save(ArtificialNeuralNetwork *neuralNetwork)
{
	FILE *file = NULL;

	file = fopen(neuralNetwork->datasetName, "w+b");
	assert(file);
	fwrite(neuralNetwork->datasetName, sizeof (char), strlen(neuralNetwork->datasetName), file);
	fwrite(&neuralNetwork->inputNeuronCount, sizeof (size_t), 1, file);
	fwrite(&neuralNetwork->outputNeuronCount, sizeof (size_t), 1, file);
	fwrite(&neuralNetwork->hiddenLayerCount, sizeof (size_t), 1, file);
	fwrite(neuralNetwork->hiddenNeuronCount, sizeof (size_t), neuralNetwork->hiddenLayerCount, file);
	for (size_t i = 0; i < neuralNetwork->hiddenLayerCount; i++) {
		Matrix_Save(neuralNetwork->hiddenLayer[i], file);
	}
	fwrite(&neuralNetwork->learningRate, sizeof (double), 1, file);
	fclose(file);
}

ArtificialNeuralNetwork *ArtificialNeuralNetwork_Load(char *datasetName)
{
	ArtificialNeuralNetwork *neuralNetwork = NULL;
	FILE *file = NULL;

	neuralNetwork = (ArtificialNeuralNetwork *)malloc(sizeof (ArtificialNeuralNetwork));
	assert(neuralNetwork);
	file = fopen(datasetName, "r+b");
	assert(file);
	neuralNetwork->datasetName = (char *)malloc((strlen(datasetName)) * sizeof (char));
	assert(neuralNetwork->datasetName);
	fread(neuralNetwork->datasetName, sizeof (char), strlen(datasetName), file);
	fread(&neuralNetwork->inputNeuronCount, sizeof (size_t), 1, file);
	fread(&neuralNetwork->outputNeuronCount, sizeof (size_t), 1, file);
	fread(&neuralNetwork->hiddenLayerCount, sizeof (size_t), 1, file);
	neuralNetwork->hiddenNeuronCount = (size_t *)malloc(neuralNetwork->hiddenLayerCount * sizeof (size_t));
	assert(neuralNetwork->hiddenNeuronCount);
	fread(neuralNetwork->hiddenNeuronCount, sizeof (size_t), neuralNetwork->hiddenLayerCount, file);
	neuralNetwork->hiddenLayer = (Matrix **)malloc(neuralNetwork->hiddenLayerCount * sizeof (Matrix *));
	assert(neuralNetwork->hiddenLayer);
	neuralNetwork->hiddenLayer[0] =
		Matrix_Load(file, neuralNetwork->inputNeuronCount, neuralNetwork->hiddenNeuronCount[0]);
	assert(neuralNetwork->hiddenLayer[0]);
	for (size_t i = 1; i < neuralNetwork->hiddenLayerCount - 1; i++) {
		neuralNetwork->hiddenLayer[i] =
			Matrix_Load(file, neuralNetwork->hiddenNeuronCount[i - 1], neuralNetwork->hiddenNeuronCount[i]);
		assert(neuralNetwork->hiddenLayer[i]);
	}
	neuralNetwork->hiddenLayer[neuralNetwork->hiddenLayerCount - 1] =
		Matrix_Load(file, neuralNetwork->hiddenNeuronCount[neuralNetwork->hiddenLayerCount - 1],
			neuralNetwork->outputNeuronCount);
	assert(neuralNetwork->hiddenLayer[neuralNetwork->hiddenLayerCount - 1]);
	fread(&neuralNetwork->learningRate, sizeof (double), 1, file);

	return neuralNetwork;
}

void ArtificialNeuralNetwork_Print(ArtificialNeuralNetwork *neuralNetwork)
{
	printf("Dataset Name : %s\n", neuralNetwork->datasetName);
	printf("Input Neuron Count : %zu\n", neuralNetwork->inputNeuronCount);
	printf("Hidden Layer Count : %zu\n", neuralNetwork->hiddenLayerCount);
	for (size_t i = 0; i < neuralNetwork->hiddenLayerCount; i++) {
		printf("Hidden Neuron Count : %zu\n", neuralNetwork->hiddenNeuronCount[i]);
		printf("Hidden Layer [%zu] :\n", i);
		Matrix_Print(neuralNetwork->hiddenLayer[i]);
	}
	printf("Output Neuron Count : %zu\n", neuralNetwork->outputNeuronCount);
}
