#include "activation-functions.h"
#include "artificial-neural-network.h"
#include "weight-initilization-functions.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	char *datasetName = "MNIST";
	size_t hiddenNeuronCount[] = {100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
	ArtificialNeuralNetwork *neuralNetwork = ArtificialNeuralNetwork_Create(datasetName, 10, 10, 10,
		hiddenNeuronCount, 10.0, getUniformDistribution);
	ArtificialNeuralNetwork_Train(neuralNetwork, neuralNetwork->hiddenLayer[0],
		neuralNetwork->hiddenLayer[neuralNetwork->hiddenLayerCount], getRelu);
	ArtificialNeuralNetwork_Save(neuralNetwork);
	ArtificialNeuralNetwork_Print(neuralNetwork);
}
