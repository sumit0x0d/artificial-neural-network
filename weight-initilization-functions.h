#ifndef WEIGHT_INITILIZATION_FUNCTIONS_H
#define WEIGHT_INITILIZATION_FUNCTIONS_H

#include <math.h>
#include <stdlib.h>

static double getWeight(double maximum, double minimum)
{
	int scale = 1000000;
	int scaledDifference = (int)((maximum - minimum) * scale);
	return minimum + (1.0 * (rand() % (int)scaledDifference) / scale);
}

static double getUniformDistribution(double fanIn, double fanOut)
{
	fanOut = fanIn;
	double minimum = -1 / sqrt(fanIn);
	double maximum = 1 / sqrt(fanOut);
	return getWeight(maximum, minimum);
}

static double getXavierNormalDistribution(double fanIn, double fanOut)
{
	double minimum = 0;
	double maximum = sqrt(2 / (fanIn + fanOut));
	return getWeight(maximum, minimum);
}

static double getXavierUniformDistribution(double fanIn, double fanOut)
{
	double minimum = -sqrt(6) / sqrt(fanIn + fanOut);
	double maximum = sqrt(6) / sqrt(fanIn + fanOut);
	return getWeight(maximum, minimum);
}

#endif
