#ifndef ACTIVATION_FUNCTIONS_H
#define ACTIVATION_FUNCTIONS_H

#include <math.h>

static double getSigmoid(double data)
{
	return 1.0 / (1 + exp(-data));
}

static double getDerivativeSigmoid(double data)
{
	return getSigmoid(data) * (1 - getSigmoid(data));
}

static double getRelu(double data)
{
	if (data < 0) {
		return 0;
	}
	return data;
}

static double getDerivativeRelu(double data)
{
	if (data < 0) {
		return 0;
	}
	return 1;
}

/* static double tanh(double data) */
/* { */
/* return (2 / (1 + exp(-data * 2))) - 1; */
/* } */

#endif
