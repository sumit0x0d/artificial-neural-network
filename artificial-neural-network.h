#ifndef ARTIFICIAL_NEURAL_NETWORK_H
#define ARTIFICIAL_NEURAL_NETWORK_H

#include <stdlib.h>

typedef struct ArtificialNeuralNetwork {
	char *datasetName;
	size_t inputNeuronCount;
	size_t outputNeuronCount;
	size_t hiddenLayerCount;
	size_t *hiddenNeuronCount;
	struct Matrix **hiddenLayer;
	double learningRate;
} ArtificialNeuralNetwork;

ArtificialNeuralNetwork *ArtificialNeuralNetwork_Create(char *datasetName, size_t inputNeuronCount,
	size_t outputNeuronCount, size_t hiddenLayerCount, size_t *hiddenNeuronCount, double learningRate,
	double (*initilizeWeight)(double fanIn, double fanOut));
void ArtificialNeuralNetwork_Destroy(ArtificialNeuralNetwork *neuralNetwork);
void ArtificialNeuralNetwork_TrainOnCsv(ArtificialNeuralNetwork *neuralNetwork, struct Matrix *inputLayer,
	struct Matrix *outputLayer, double (*activate)(double weight));
void ArtificialNeuralNetwork_Predict(ArtificialNeuralNetwork *neuralNetwork, struct Matrix *inputLayer);
void ArtificialNeuralNetwork_Save(ArtificialNeuralNetwork *neuralNetwork);
ArtificialNeuralNetwork *ArtificialNeuralNetwork_Load(char* datasetName);
void ArtificialNeuralNetwork_Print(ArtificialNeuralNetwork *neuralNetwork);

#endif
