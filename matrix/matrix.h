#ifndef MATRIX_H
#define MATRIX_H

#include <stddef.h>
#include <stdio.h>

typedef struct Matrix {
	double **base;
	size_t rowCount;
	size_t columnCount;
} Matrix;

Matrix *Matrix_Create(size_t rowCount, size_t columnCount);
void Matrix_Destroy(Matrix *matrix);
void Matrix_Initilization(Matrix *matrix, double weight);
void Matrix_ScalarMultiplication(Matrix *matrix, double weight);
void Matrix_ScalarAddition(Matrix *matrix, double weight);
void Matrix_WeightInitilization(Matrix *matrix, double fanIn, double fanOut,
	double (*initilizeWeight)(double fanIn, double fanOut));
void Matrix_Activation(Matrix *matrix, double (*activate)(double weight));
Matrix *Matrix_Transposition(Matrix *matrix);
Matrix *Matrix_Multiplication(Matrix *matrix1, Matrix *matrix2);
Matrix *Matrix_Addition(Matrix *matrix1, Matrix *matrix2);
Matrix *Matrix_Subtraction(Matrix *matrix1, Matrix *matrix2);
Matrix *Matrix_HadamardProduct(Matrix *matrix1, Matrix *matrix2);
Matrix *Matrix_RowVectorization(Matrix *matrix);
Matrix *Matrix_ColumnVectorization(Matrix *matrix);
void Matrix_Softmax(Matrix *matrix);
void Matrix_Maxout(Matrix *matrix);
void Matrix_DeepCopy(Matrix *matrix1, Matrix *matrix2);
void Matrix_Save(Matrix *matrix, FILE *file);
Matrix *Matrix_Load(FILE *file, size_t rowCount, size_t columnCount);
Matrix *Matrix_LoadFromCsv(FILE *file);
Matrix *Matrix_LoadFromPgm(FILE *file);
void Matrix_Print(Matrix *matrix);

#endif
