#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include "matrix.h"

Matrix *Matrix_Create(size_t rowCount, size_t columnCount)
{
	Matrix *matrix = (Matrix *)malloc(sizeof (Matrix));
	assert(matrix);
	matrix->base = (double **)malloc(rowCount * sizeof (double *));
	assert(matrix->base);
	for (size_t i = 0; i < rowCount; i++) {
		matrix->base[i] = (double *)malloc(columnCount * sizeof (double));
		assert(matrix->base[i]);
	}
	matrix->rowCount = rowCount;
	matrix->columnCount = columnCount;
	return matrix;
}

void Matrix_Destroy(Matrix *matrix)
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		free(matrix->base[i]);
	}
	free(matrix->base);
	free(matrix);
}

void Matrix_Initilization(Matrix *matrix, double weight)
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[j][i] = weight;
		}
	}
}

Matrix *Matrix_Transposition(Matrix *matrix)
{
	Matrix *matrixTranspose = Matrix_Create(matrix->columnCount, matrix->rowCount);
	assert(matrixTranspose);
	for (size_t i = 0; i < matrixTranspose->rowCount; i++) {
		for (size_t j = 0; j < matrixTranspose->columnCount; j++) {
			matrixTranspose->base[j][i] = matrix->base[i][j];
		}
	}
	return matrixTranspose;
}

Matrix *Matrix_Multiplication(Matrix *matrix1, Matrix *matrix2)
{
	if (matrix1->columnCount != matrix2->rowCount) {
		return NULL;
	}
	Matrix *matrix = Matrix_Create(matrix1->rowCount, matrix2->columnCount);
	assert(matrix);
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			for (size_t k = 0; k < matrix1->columnCount; k++) {
				matrix->base[i][j] += matrix1->base[i][k] * matrix2->base[k][j];
			}
		}
	}
	return matrix;
}

Matrix *Matrix_Addition(Matrix *matrix1, Matrix *matrix2)
{
	assert(matrix1->rowCount == matrix2->rowCount);
	assert(matrix1->columnCount == matrix2->columnCount);
	Matrix *matrix = Matrix_Create(matrix1->rowCount, matrix1->columnCount);
	assert(matrix);
	for (size_t i = 0; i < matrix1->rowCount; i++) {
		for (size_t j = 0; j < matrix2->columnCount; j++) {
			matrix1->base[i][j] = matrix1->base[i][j] + matrix2->base[i][j];
		}
	}
	return matrix;
}

Matrix *Matrix_Subtraction(Matrix *matrix1, Matrix *matrix2)
{
	assert(matrix1->rowCount == matrix2->rowCount);
	assert(matrix1->columnCount == matrix2->columnCount);
	Matrix *matrix = Matrix_Create(matrix1->rowCount, matrix1->columnCount);
	assert(matrix);
	for (size_t i = 0; i < matrix1->rowCount; i++) {
		for (size_t j = 0; j < matrix2->columnCount; j++) {
			matrix1->base[i][j] = matrix1->base[i][j] - matrix2->base[i][j];
		}
	}
	return matrix;
}

Matrix *Matrix_HadamardProduct(Matrix *matrix1, Matrix *matrix2)
{
	assert(matrix1->rowCount == matrix2->rowCount);
	assert(matrix1->columnCount == matrix2->columnCount);
	Matrix *matrix = Matrix_Create(matrix1->rowCount, matrix1->columnCount);
	assert(matrix);
	for (size_t i = 0; i < matrix1->rowCount; i++) {
		for (size_t j = 0; j < matrix2->columnCount; j++) {
			matrix->base[i][j] = matrix1->base[i][j] * matrix2->base[i][j];
		}
	}
	return matrix;
}

void Matrix_ScalarMultiplication(Matrix *matrix, double weight)
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[i][j] *= weight;
		}
	}
}

void Matrix_ScalarAddition(Matrix *matrix, double weight)
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[i][j] += weight;
		}
	}
}

void Matrix_WeightInitilization(Matrix *matrix, double fanIn, double fanOut,
	double (*initilizeWeight)(double fanIn, double fanOut))
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[i][j] = initilizeWeight(fanIn, fanOut);
		}
	}
}

void Matrix_Activation(Matrix *matrix, double (*activate)(double weight))
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[i][j] = activate(matrix->base[i][j]);
		}
	}
}

void Matrix_DeepCopy(Matrix *matrix1, Matrix *matrix2)
{
	for (size_t i = 0; i < matrix1->rowCount; i++) {
		for (size_t j = 0; j < matrix1->columnCount; j++) {
			matrix1->base[i][j] = matrix2->base[i][j];
		}
	}
}

Matrix *Matrix_RowVectorization(Matrix *matrix)
{
	Matrix *matrixVectorize = Matrix_Create(1, matrix->rowCount * matrix->columnCount);
	assert(matrixVectorize);
	for (size_t i = 0; i < matrix->rowCount; i++)  {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrixVectorize->base[0][(i * matrix->columnCount) + j] = matrix->base[i][j];
		}
	}
	return matrixVectorize;
}

Matrix *Matrix_ColumnVectorization(Matrix *matrix)
{
	Matrix *matrixVectorize = Matrix_Create(matrix->rowCount * matrix->columnCount, 1);
	assert(matrixVectorize);
	for (size_t i = 0; i < matrix->rowCount; i++)  {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrixVectorize->base[(i * matrix->columnCount) + j][0] = matrix->base[i][j];
		}
	}
	return matrixVectorize;
}

void Matrix_Softmax(Matrix *matrix)
{
	double sum = 0;
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			sum += exp(matrix->base[i][j]);
		}
	}
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[i][j] = exp(matrix->base[i][j]) / sum;
		}
	}
}

void Matrix_Maxout(Matrix *matrix)
{
	double sum = 0;
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			sum += exp(matrix->base[i][j]);
		}
	}
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			matrix->base[i][j] = exp(matrix->base[i][j]) / sum;
		}
	}
}

void Matrix_Save(Matrix *matrix, FILE* file)
{
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			fwrite(&matrix->base[i][j], sizeof (double), 1, file);
		}
	}
}

Matrix *Matrix_Load(FILE* file, size_t rowCount, size_t columnCount)
{
	Matrix *matrix = Matrix_Create(rowCount, columnCount);
	assert(matrix);
	for (size_t i = 0; i < rowCount; i++) {
		for (size_t j = 0; j < columnCount; j++) {
			fread(&matrix->base[i][j], sizeof (double), 1, file);
		}
	}
	return matrix;
}

// Matrix *Matrix_loadfromvsv(FILE* file)
// {
// Matrix *matrix = MatrixNew(rowCount, columnCount);
// assert(matrix) {
// return NULL;
// }
// return matrix;
// }

Matrix *Matrix_LoadFromPgm(FILE* file)
{
	size_t rowCount;
	size_t columnCount;
	fread(NULL, sizeof (char), 2, file);
	fread(&rowCount, sizeof (size_t), 1, file);
	fread(&columnCount, sizeof (size_t), 1, file);
	Matrix *matrix = Matrix_Create(rowCount, columnCount);
	assert(matrix);
	return matrix;
}

void Matrix_Print(Matrix *matrix)
{
	printf("Row Count : %zu\n", matrix->rowCount);
	printf("Column Count : %zu\n", matrix->columnCount);
	for (size_t i = 0; i < matrix->rowCount; i++) {
		for (size_t j = 0; j < matrix->columnCount; j++) {
			printf("%f ", matrix->base[i][j]);
		}
		printf("\n");
	}
}
