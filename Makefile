CC = gcc

TARGET = main

CFLAGS = -std=c11 -O0 -g -Wall -Wpedantic -Wextra -lm
# CFLAGS = -std=c99 -O0 -g -Wall -Wpedantic -Wextra -Werror
# CFLAGS = -std=c99 -O0 -g -Wall -Wpedantic -Wextra -Werror -fsanitize=address

all:
	$(CC) main.c -o $(TARGET) \
	artificial-neural-network.c \
	matrix/matrix.c \
	$(CFLAGS)

